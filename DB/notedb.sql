-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2015 at 04:02 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `notedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `id` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8,
  `type` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `modifydate` datetime DEFAULT NULL,
  `islock` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`id`, `content`, `type`, `createddate`, `modifydate`, `islock`) VALUES
('public', 'http://localhost:8080/ghichu/public\n\nhttp://localhost:8080/ghichu/public\n\nhttp://localhost:8080/ghichu/public\n\nvar type = document.getElementById(''typeSelector'').value;\n\nvar type = document.getElementById(''typeSelector'').value;\n\nvar type = document.getElementById(''typeSelector'').value;', 2, '2015-08-15 00:00:00', '2015-08-16 00:00:00', 0),
('trial', '<H1>Using Sessions to Track Users</H1>\n        Session ID: <%=session.getId()%>\n        <BR>\n        Session creation time: <%=new Date(session.getCreationTime())%>\n        <BR>\n        Last accessed time: <%=new Date(session.getLastAccessedTime())%>\n        <BR>\n        Number of times you''ve been here: <%=counter%> \n\n<H1>Using Sessions to Track Users</H1>\n        Session ID: <%=session.getId()%>\n        <BR>\n        Session creation time: <%=new Date(session.getCreationTime())%>\n        <BR>\n        Last accessed time: <%=new Date(session.getLastAccessedTime())%>\n        <BR>\n        Number of times you''ve been here: <%=counter%> ', 1, '2015-08-12 00:00:00', '2015-09-07 00:00:00', 0),
('tuoitre', 'Hi!\n\n\n---Test...\n\n\n\nhttp://localhost:8080/ghichu/public  oncut="onTextChange()" onpaste="onTextChange()"\n\n\n\nhttp://localhost:8080/ghichu/public  oncut="onTextChange()" onpaste="onTextChange()"\n\nhttp://localhost:8080/ghichu/public  oncut="onTextChange()" onpaste="onTextChange()"\n\n\n\n$(''#typeSelector'')\n\neditor.setReadOnly(false);\n\n\n$(''#typeSelector'')\n\n$(''#typeSelector'')', 2, '2015-07-29 00:00:00', '2015-08-17 00:00:00', 1),
('tuoitreuit', '<%@page import = "java.util.*" session="true"%>\n<HTML> \n    <HEAD>\n        <TITLE>Using Sessions to Track Users</TITLE>\n    </HEAD> \n\n    <BODY>\n        <% \n        Integer counter = (Integer)session.getAttribute("counter");\n        if (counter == null) {\n            counter = new Integer(1);\n        } else {\n            counter = new Integer(counter.intValue() + 1);\n        }\n\n        session.setAttribute("counter", counter);\n        %>\n        <H1>Using Sessions to Track Users</H1>\n        Session ID: <%=session.getId()%>\n        <BR>\n        Session creation time: <%=new Date(session.getCreationTime())%>\n        <BR>\n        Last accessed time: <%=new Date(session.getLastAccessedTime())%>\n        <BR>\n        Number of times you''ve been here: <%=counter%> \n        \n        <H1>Using Sessions to Track Users</H1>\n        Session ID: <%=session.getId()%>\n        <BR>\n        Session creation time: <%=new Date(session.getCreationTime())%>\n        <BR>\n        Last accessed time: <%=new Date(session.getLastAccessedTime())%>\n        <BR>\n        Number of times you''ve been here: <%=counter%> \n        \n        <H1>Using Sessions to Track Users</H1>\n        Session ID: <%=session.getId()%>\n        <BR>\n        Session creation time: <%=new Date(session.getCreationTime())%>\n        <BR>\n        Last accessed time: <%=new Date(session.getLastAccessedTime())%>\n        <BR>\n        Number of times you''ve been here: <%=counter%> \n    </BODY> \n</HTML>', 1, '2015-09-07 00:00:00', '2015-09-07 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notepass`
--

CREATE TABLE IF NOT EXISTS `notepass` (
  `noteId` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `updatetime` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notepass`
--

INSERT INTO `notepass` (`noteId`, `password`, `updatetime`) VALUES
('public', 'dead', '2015-08-15'),
('tuoitre', 'tuoitre', '2015-08-16'),
('tuoitreuit', 'tuoitreuit', '2015-09-07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `note`
--
ALTER TABLE `note`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notepass`
--
ALTER TABLE `notepass`
 ADD PRIMARY KEY (`noteId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
