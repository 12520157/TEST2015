package com.antt.testing;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.antt.database.model.Note;

public class TestNote {
	Note note, note1, note2;
	
	String noteid;
	String content;
	Integer type;
	boolean lock;
	Date createddate;
	Date modifydate;

	@Before
	public void setUp() throws Exception {
		noteid = "uit12520157";
		content = "TESTING";
		type = 0;
		lock = false;
		createddate = new Date(new java.util.Date().getTime());
		modifydate = new Date(new java.util.Date().getTime());
		note = new Note(noteid, content, type, createddate, modifydate, lock);
		note1 = new Note();
		note2 = new Note(noteid, content, type, createddate, modifydate);
	}

	@After
	public void tearDown() throws Exception {
		note = note1 = note2 = null;		
	}	
	
	@Test
	public void testGetNoteid() {		
		assertEquals(noteid, note.getNoteid());
	}

	@Test
	public void testSetNoteid() {
		noteid = "uit12520069";
		note.setNoteid(noteid);
		assertEquals(noteid, note.getNoteid());
	}

	@Test
	public void testGetContent() {
		assertEquals(content, note.getContent());
	}

	@Test
	public void testSetContent() {
		content = "TESTING CONTENT!";
		note.setContent(content);
		assertEquals(content, note.getContent());
	}

	@Test
	public void testGetType() {
		assertEquals(type, note.getType());
	}

	@Test
	public void testSetType() {
		type = 1;
		note.setType(type);
		assertEquals(type, note.getType());
	}

	@Test
	public void testGetCreateddate() {
		assertEquals(createddate, note.getCreateddate());
	}

	@Test
	public void testSetCreateddate() {
		createddate = new Date(new java.util.Date().getTime());
		note.setCreateddate(createddate);
		assertEquals(createddate, note.getCreateddate());
	}

	@Test
	public void testGetModifydate() {
		assertEquals(modifydate, note.getModifydate());
	}

	@Test
	public void testSetModifydate() {
		modifydate = new Date(new java.util.Date().getTime());
		note.setModifydate(modifydate);
		assertEquals(modifydate, note.getModifydate());
	}

	@Test
	public void testIsLock() {
		assertEquals(lock, note.isLock());
	}

	@Test
	public void testSetLock() {
		lock = true;
		note.setLock(lock);
		assertEquals(lock, note.isLock());
	}

	@Test
	public void testNote() {
		//test Contructor
		assertNotNull(note1);
	}

	@Test
	public void testNoteStringStringIntDateDate() {
		//test Contructor
		assertNotNull(note2);
	}

	@Test
	public void testNoteStringStringIntDateDateBoolean() {
		//test Contructor
		assertNotNull(note);
	}

	@Test
	public void testToString() {
		assertNotNull(note.toString());
	}

}
