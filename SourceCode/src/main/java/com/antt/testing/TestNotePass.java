package com.antt.testing;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.antt.database.model.NotePass;

public class TestNotePass {
	
	NotePass notepass, notepass2;
	
	String noteId;
	String password;
	Date updateTime;

	@Before
	public void setUp() throws Exception {
		noteId = "uit12520157";
		password = "uit";
		updateTime = new Date(new java.util.Date().getTime());
		notepass = new NotePass(noteId, password, updateTime);
		notepass2 = new NotePass();
	}

	@After
	public void tearDown() throws Exception {
		notepass = notepass2 = null;
	}

	@Test
	public void testNotePass() {
		//test Contructor
		assertNotNull(notepass2);
	}

	@Test
	public void testNotePassStringStringDate() {
		//test Contructor
		assertNotNull(notepass);
	}

	@Test
	public void testGetNoteId() {
		assertEquals(noteId, notepass.getNoteId());
	}

	@Test
	public void testSetNoteId() {
		noteId = "uit12520069";
		notepass.setNoteId(noteId);
		assertEquals(noteId, notepass.getNoteId());
	}

	@Test
	public void testGetPassword() {
		assertEquals(password, notepass.getPassword());
	}

	@Test
	public void testSetPassword() {
		password = "uit1252";
		notepass.setPassword(password);
		assertEquals(password, notepass.getPassword());
	}

	@Test
	public void testGetUpdateTime() {
		assertEquals(updateTime, notepass.getUpdateTime());
	}

	@Test
	public void testSetUpdateTime() {
		updateTime = new Date(new java.util.Date().getTime());
		notepass.setUpdateTime(updateTime);
		assertEquals(updateTime, notepass.getUpdateTime());
	}

}
