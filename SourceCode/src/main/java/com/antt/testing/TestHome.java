package com.antt.testing;

import static net.sourceforge.jwebunit.junit.JWebUnit.assertTitleEquals;
import static net.sourceforge.jwebunit.junit.JWebUnit.beginAt;
import static net.sourceforge.jwebunit.junit.JWebUnit.setBaseUrl;
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;

public class TestHome {

	@Before
	public void setUp() throws Exception {
		setBaseUrl("http://localhost:8080/ghichu");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testHome2() {
		beginAt("/");
		assertTitleEquals("Ghi nháp - Trang chủ");
	}

	@Test
	public void testHome() throws IOException, SAXException {
		WebConversation conversation = new WebConversation();
        WebRequest request = new GetMethodWebRequest("http://localhost:8080/ghichu/12520157");
        WebResponse response = conversation.getResponse(request);
		
        System.out.println(response.getTitle());
		assertEquals("Ghi nháp", response.getTitle());		      
	}

	@Test
	public void testGetList() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetPassword() {
		fail("Not yet implemented");
	}

	@Test
	public void testUnSetPassword() {
		fail("Not yet implemented");
	}

	@Test
	public void testToEdit() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCanvasContent() {
		fail("Not yet implemented");
	}

}
