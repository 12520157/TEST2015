package com.antt.testing;

import static net.sourceforge.jwebunit.junit.JWebUnit.assertTitleEquals;
import static net.sourceforge.jwebunit.junit.JWebUnit.beginAt;
import static net.sourceforge.jwebunit.junit.JWebUnit.setBaseUrl;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.antt.database.dao.NoteDAO;
import com.antt.database.dao.NoteDAOImpl;

public class TestNoteDAOImpl {
	
	NoteDAO notedao;

	@Before
	public void setUp() throws Exception {		
		setBaseUrl("http://localhost:8080/ghichu");		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNoteDAOImpl() {
		fail("Not yet implemented");
	}

	@Test
	public void testList() {
		beginAt("/");
		assertTitleEquals("Ghi nháp - Trang chủ");
		
	}

	@Test
	public void testAddNote() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindNote() {
		beginAt("/");
		assertTitleEquals("Ghi nháp - Trang chủ");
		assertEquals(false, notedao.findNote("12520157uit"));
	}

	@Test
	public void testEditNote() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetLock() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetLastestNotes() {
		beginAt("/");
		assertTitleEquals("Ghi nháp - Trang chủ");
		assertNotNull(notedao.getLastestNotes());
	}

}
